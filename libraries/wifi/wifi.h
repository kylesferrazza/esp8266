#ifndef WIFI
#define WIFI

#include <ESP8266WiFi.h>
#include <wifipass.h>

void connectWifi() {
  Serial.print("Connecting to wifi");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
#ifdef MATRIX
  boolean colon = true;
#endif
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
#ifdef MATRIX
    matrix.drawColon(colon);
    colon = !colon;
    matrix.writeDisplay();
#endif
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}
#endif
