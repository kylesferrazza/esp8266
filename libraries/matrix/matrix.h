#ifndef MATRIX
#define MATRIX

#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"

Adafruit_7segment matrix = Adafruit_7segment();

void displayNum(int i, int = DEC);

void clearMatrix() {
  matrix.clear();
  matrix.writeDisplay();
}

void setupMatrix() {
  matrix.begin(0x70);
  clearMatrix();
}

void displayNum(int i, int base) {
  Serial.print("Displaying '");
  Serial.print(i);
  Serial.println("'.");
  matrix.print(i, base);
  matrix.writeDisplay();
}

#endif
