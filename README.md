# ESP8266

Some libraries and examples I have made for my [Adafruit Feather HUZZAH with ESP8266](huzzah), using the [Arduino IDE](arduino).

[huzzah]: https://www.adafruit.com/product/2821
[arduino]: https://www.arduino.cc/
