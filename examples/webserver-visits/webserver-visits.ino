/*
 * Start a webserver, count the number of visits.
 * Every visit, show the updated number on the display,
 * and in the resulting HTML page.
 */

#include <ESP8266WebServer.h>
#include <ESP8266NetBIOS.h>

#include <matrix.h>
#include <wifi.h>

ESP8266WebServer wwwserver(80);
String content;

static int visits = 0;

static void handleRoot(void) {
  char v[32];
  visits++;
  itoa(visits, v, 10);
  displayNum(visits);
  content = F("<!DOCTYPE HTML>\n<html>");
  content += F("<h1>Visits: ");
  content += FPSTR(v);
  content += F("</h1>");
  content += F("</html>");

  wwwserver.send(200, F("text/html"), content);
}

void setup() {
  Serial.begin(115200);
  setupMatrix();
  connectWifi();

  wwwserver.on("/", handleRoot);
  wwwserver.begin();

  NBNS.begin("ESP");
  displayNum(visits);
}

void loop() {
  wwwserver.handleClient();
}
