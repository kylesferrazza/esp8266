/*
 * Get IP Address from icanhazip.com, print it on display one segment at a time.
 */

#include <matrix.h>
#include <wifi.h>

const char* host = "icanhazip.com";
const uint16_t port = 80;

void setup() {
  Serial.begin(115200);
  setupMatrix();
  connectWifi();
}

void loop() {
  Serial.print("connecting to ");
  Serial.print(host);
  Serial.print(':');
  Serial.println(port);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  if (!client.connect(host, port)) {
    Serial.println("connection failed");
    delay(5000);
    return;
  }

  // This will send a string to the server
  Serial.println("sending data to server");
  client.println("GET /");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      delay(60000);
      return;
    }
  }

  int seg = 0;
  Serial.println("receiving from remote server");
  while (client.available()) {
    char ch = static_cast<char>(client.read());
    Serial.println();
    if (isdigit(ch)) {
      seg *= 10;
      seg += (ch - '0');
    } else {
      Serial.print("next seg: ");
      Serial.println(seg);
      displayNum(seg);
      delay(1000);
      seg = 0;
    }
    Serial.print(ch);
  }

  // Close the connection
  Serial.println();
  Serial.println("closing connection");
  client.stop();

  delay(10000);
}
